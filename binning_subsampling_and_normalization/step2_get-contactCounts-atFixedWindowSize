#!/bin/bash

set -o nounset
set -o pipefail
set -o errexit 

paper=Plasmo-Sexual2014
DATADIR=/net/noble/vol2/home/ferhatay/public_html/proj/2014ATACseq/data/cleanedPairs
w=$1 #10000
outdir=/net/noble/vol2/home/ferhatay/public_html/proj/2013publishedHiCdata/data/$paper/interactionCounts/beforeICE/$w
mkdir -p $outdir
lowDistThres=1000  # make this 1kb to preserve diagonal contacts
#lowDistThres=3000 

################################
## Outline of each awk line ##
  #read
	#eliminate random chrs etc -- MAY change from organism to organism
	#eliminate very close intrachr counts -- MAY change from lib to lib, and org to org
	#bin using the window size 
	#sort-uniq to count 
	#print
	#compress
################################

ref=pfal3D7
# Replicates combined

lib=HiC_Pf-Ring-HP1-KD-Combined
 	jobfile=$lib-countContacts-fixedSizeWindows-w$w.job
    echo    "#!/bin/bash -ex" > $jobfile; echo    "#$ -cwd" >> $jobfile; echo    "#$ -l longjob=TRUE" >> $jobfile;  echo    "#$ -l h_vmem=8G" >> $jobfile;
	echo    "#$ -l rhel=6" >> $jobfile; echo    "#$ -j y" >> $jobfile; echo    "source ~/.bashrc" >> $jobfile; echo    hostname >> $jobfile; 

	echo 	"zcat $DATADIR/HiC_Pf-HP1-KD/L?.gz  $DATADIR/HiC_Ring-HP1-KO/L?.gz \\"  >> $jobfile
	echo	"| awk 'length(\$3)<=5 && length(\$7)<=5 {print}' | grep -v chrM | grep -v chrP \\" >> $jobfile
	echo	"| awk '\$3!=\$7 || (\$3==\$7 && (\$4-\$8> t || \$8-\$4> t)) {print}' t=$lowDistThres \\" >> $jobfile
	echo	"| awk '{print \$3,int(\$4/r)*r,\$7,int(\$8/r)*r}' r=$w \\" >> $jobfile
	echo	"| awk '{if(\$1<\$3 || (\$1==\$3 && \$2<=\$4)) print \$1,\$2,\$3,\$4; else print \$3,\$4,\$1,\$2}' | sort | uniq -c \\" >> $jobfile
	echo	"| awk 'OFS=\"\\t\"{print \$2,\$3+r/2,\$4,\$5+r/2,\$1}' r=$w > $outdir/${lib} 	" >> $jobfile
	echo	"gzip $outdir/${lib}" >> $jobfile
	
	echo	"	" >> $jobfile;chmod +x $jobfile
    qsub -N $lib-countContacts-fixedSizeWindows-w$w $jobfile


lib=HiC_Pf-SPZ-Combined 
 	jobfile=$lib-countContacts-fixedSizeWindows-w$w.job
    echo    "#!/bin/bash -ex" > $jobfile; echo    "#$ -cwd" >> $jobfile; echo    "#$ -l longjob=TRUE" >> $jobfile;  echo    "#$ -l h_vmem=8G" >> $jobfile;
	echo    "#$ -l rhel=6" >> $jobfile; echo    "#$ -j y" >> $jobfile; echo    "source ~/.bashrc" >> $jobfile; echo    hostname >> $jobfile; 

	echo 	"zcat $DATADIR/HiC_Pf-SPZ/L?.gz  $DATADIR/HiC_SPZ-Covaris/L?.gz \\"  >> $jobfile
	echo	"| awk 'length(\$3)<=5 && length(\$7)<=5 {print}' | grep -v chrM | grep -v chrP \\" >> $jobfile
	echo	"| awk '\$3!=\$7 || (\$3==\$7 && (\$4-\$8> t || \$8-\$4> t)) {print}' t=$lowDistThres \\" >> $jobfile
	echo	"| awk '{print \$3,int(\$4/r)*r,\$7,int(\$8/r)*r}' r=$w \\" >> $jobfile
	echo	"| awk '{if(\$1<\$3 || (\$1==\$3 && \$2<=\$4)) print \$1,\$2,\$3,\$4; else print \$3,\$4,\$1,\$2}' | sort | uniq -c \\" >> $jobfile
	echo	"| awk 'OFS=\"\\t\"{print \$2,\$3+r/2,\$4,\$5+r/2,\$1}' r=$w > $outdir/${lib} 	" >> $jobfile
	echo	"gzip $outdir/${lib}" >> $jobfile
	
	echo	"	" >> $jobfile;chmod +x $jobfile
    qsub -N $lib-countContacts-fixedSizeWindows-w$w $jobfile


# Replicates separately
#for lib in HiC_GAM_I HiC_GAM_II HiC_SPZ; do
#for lib in HiC_GAM-Covaris HiC_SPZ-Covaris HiC_Troph-AluI HiC_Troph-Covaris; do
#for lib in HiC_Ring-HP1-KO HiC_Ring-HP1-WT; do
#for lib in allCombinedFragFiltered; do
#for lib in HiC_Pf-Early-GAM HiC_Pf-HP1-KD HiC_Pf-SPZ; do
for lib in HiC_Pf-Early-GAM HiC_Pf-HP1-KD HiC_Pf-SPZ HiC_Ring-HP1-KO HiC_Ring-HP1-WT HiC_GAM_I HiC_GAM_II \
	HiC_SPZ HiC_GAM-Covaris HiC_SPZ-Covaris HiC_Troph-AluI HiC_Troph-Covaris; do

 	jobfile=$lib-countContacts-fixedSizeWindows-w$w.job
    echo    "#!/bin/bash -ex" > $jobfile; echo    "#$ -cwd" >> $jobfile; echo    "#$ -l longjob=TRUE" >> $jobfile;  echo    "#$ -l h_vmem=8G" >> $jobfile;
	echo    "#$ -l rhel=6" >> $jobfile; echo    "#$ -j y" >> $jobfile; echo    "source ~/.bashrc" >> $jobfile; echo    hostname >> $jobfile; 

	echo 	"zcat $DATADIR/${lib}/L?.gz \\"  >> $jobfile
	echo	"| awk 'length(\$3)<=5 && length(\$7)<=5 {print}' | grep -v chrM | grep -v chrP \\" >> $jobfile
	echo	"| awk '\$3!=\$7 || (\$3==\$7 && (\$4-\$8> t || \$8-\$4> t)) {print}' t=$lowDistThres \\" >> $jobfile
	echo	"| awk '{print \$3,int(\$4/r)*r,\$7,int(\$8/r)*r}' r=$w \\" >> $jobfile
	echo	"| awk '{if(\$1<\$3 || (\$1==\$3 && \$2<=\$4)) print \$1,\$2,\$3,\$4; else print \$3,\$4,\$1,\$2}' | sort | uniq -c \\" >> $jobfile
	echo	"| awk 'OFS=\"\\t\"{print \$2,\$3+r/2,\$4,\$5+r/2,\$1}' r=$w > $outdir/${lib} 	" >> $jobfile
	echo	"gzip $outdir/${lib}" >> $jobfile
	
	echo	"	" >> $jobfile;chmod +x $jobfile
    qsub -N $lib-countContacts-fixedSizeWindows-w$w $jobfile
done




exit

